<?php

/***
*
* Drush Export Content Type. Imports/Exports content types
* @version 0.2 - 20130410
* @author Leandro Vázquez Cervantes <leandro@leandro.org>
* @copyright Copyright 2013  - Leandro Vázquez Cervantes
* @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GPL License
* @package export_ct
*
*/

require_once "spyc.php";

function export_ct_drush_command()
{
  $items = array();

  // LIST

  $items['export-ct-list'] = array(
    'description' => "List current content types.",
    'examples' => array(
      'drush export_ct-list' => 'List all the content types.',
    ) ,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  // DESC

  $items['export-ct-desc'] = array(
    'description' => "Describe fields of a given content type",
    'arguments' => array(
      'content_type' => 'Content type you would like to be described.',
    ) ,
    'examples' => array(
      'drush export_ct-desc content_type ' => 'Describe fields of a given content type',
    ) ,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  // EXPORT TO YAML

  $items['export-ct-export'] = array(
    'description' => "Exports fields of a given content type to a txt file",
    'arguments' => array(
      'content_type' => 'Content type you would like to be exported to a txt file.',
      'path' => 'Path desired to save the txt file',
    ) ,
    'examples' => array(
      'drush export_ct-export content_type /tmp' => 'Exports fields of a given content type to a txt file',
    ) ,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  // IMPORT

  $items['export-ct-import'] = array(
    'description' => "Create content type from exported fields in txt file",
    'arguments' => array(
      'content_type' => 'Content type you would like to create from a txt file. DANGER: input must be a non-existent content type, because if exists previously it will be deleted!!',
      'path' => 'Absolute path to the txt file that incluedes the fields to be imported',
    ) ,
    'examples' => array(
      'drush export_ct-import content_type ' => 'Create content type from exported fields in txt file',
    ) ,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  // DELETE

  $items['export-ct-delete'] = array(
    'description' => "This command deletes a content type. Use with precaution.",
    'arguments' => array(
      'content_type' => 'Content type you would like to be deleted.',
    ) ,
    'examples' => array(
      'drush export_ct-delete content_type ' => 'Deletes definitively a content type',
    ) ,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  return $items;
}

function export_ct_drush_help($section)
{
  switch ($section) {
		case 'drush:export-ct':
		  return dt("This is for cloning content types to a YAML file, afterward you can edit it and import the structure in another or the same site");
		case 'drush:export-ct-list':
		  return dt("This command will list your content types.");
		case 'drush:export-ct-desc':
		  return dt("This command will list the fields of the desired content type");
		case 'drush:export-ct-export':
		  return dt("Exports fields of a given content type to a txt file");
		case 'drush:export-ct-import':
		  return dt("Create content type from exported fields in txt file. DANGER: input must be a non-existent content type, because if exists previously it will be deleted!!");
		case 'drush:export-ct-delete':
		  return dt("This command deletes definitively a  content type from your database. Use with caution.");
  }
}

function drush_export_ct_list()
{
  $content_types = node_type_get_types();
  $rows[] = array( dt('Name') , dt('Machine name') );
  $rows[] = array( '====', '============' );
  foreach($content_types as $k => $v) {
    $rows[] = array( $v->name, $k );
  }

  drush_print_table($rows);
}

function drush_export_ct_desc($content_type)
{
  $instances = field_info_instances('node', $content_type);
  $extra_fields = field_info_extra_fields('node', $content_type, 'form');
  if (empty($instances)) {
    return drush_set_error('ERROR_CODE', dt('content ' . $content_type . ' has no fields or it doesn\'t exists'));
  }

  foreach($instances as $name => $instance) {
    $field = field_info_field($instance['field_name']);
    $fields[$instance['field_name']] = array(
      'field_name' => $instance['field_name'],
      'title' => $instance['label'],
      'type' => $field['type'],
      'weight' => $instance['widget']['weight'],
    );
  }

  $res = array_merge($fields, $extra_fields);
  usort($res, 'cmp'); //order by weight
  $rows[] = array( dt('title') , dt('field_name') , dt('type') , dt('weight') );
  $rows[] = array( '=====', '==========', '====', '======' );
  foreach($res as $r) {
    $title = ($r['title']) ? : $r['label'];
    $rows[] = array( $title, $r['field_name'], $r['type'], $r['weight'] );
  }

  drush_print_table($rows);
}

function cmp($x, $y)
{
  if ($x['weight'] == $y['weight']) return 0;
  else if ($x['weight'] > $y['weight']) return 1;
  else return -1;
}

function drush_export_ct_export($content_type, $path = null)
{
  $path = ($path) ? $path . "/" . $content_type . ".export.txt" : null;
  $instances = field_info_instances('node', $content_type);
  $fields = array();
  foreach($instances as $name => $instance) {
    $field = field_info_field($instance['field_name']);
    $fields[$instance['field_name']] = array(
      'field_name' => $instance['field_name'],
      'title' => $instance['label'],
      'type' => $field['type'],
      'weight' => $instance['widget']['weight'],
    );
  }

  if (empty($instances)) {
    return drush_set_error('ERROR_CODE', dt($content_type . ' has no fields'));
  }

  unset($fields['body']);
  $res = array(
    'fields' => $fields,
    'instances' => $instances,
    'definition' => array(
      'machine_name' => 'change_me',
      'name' => 'name change me',
      'label' => 'Title',
      'desc' => 'description change me'
    )
  );
  save_yaml($res, $content_type, $path);
}

function save_yaml($content, $name_content, $path = null)
{
  $content = Spyc::YAMLDump($content);
  file_put_contents($path, $content);
  drush_print($name_content . " fields structure copied to " . $path);
}

function drush_export_ct_import($content_type_name, $file)
{
  $t = get_t();
  if (file_exists($file)) {
    $xml = spyc_load_file($file);

    // define the node type

    $node_type = array(
      'type' => $content_type_name,
      'name' => $t($xml['definition']['name']) ,
      'base' => 'node_content',
      'title_label' => $t($xml['definition']['label']) ,
      'description' => $t($xml['definition']['desc']) ,
      'custom' => TRUE,
    );

    // set other node defaults not declared above

    $content_type = node_type_set_defaults($node_type);

    // add the body field
    // node_add_body_field($content_type, $t($desc));
    
    
    
    // save the content type

    $_ = field_info_instances('node', $content_type_name);
    if (!empty($_)) {
      if (false == drush_confirm($content_type_name . ' will be overwritten. Are you sure you want to continue?', $indent = 0)) return;
    }

    node_type_delete($content_type_name); //DANGER!!
    node_type_save($content_type);

    // add peristant variables that control settings

    variable_set('additional_settings__active_tab_' . $content_type_name, 'edit-menu');
    variable_set('node_preview_' . $content_type_name, 2);
    variable_set('node_options_' . $xml['definition']['machine_name'], array(
      0 => 'status',
      1 => 'promote'
    ));
    variable_set('node_submitted_' . $xml['definition']['machine_name'], 0);
    variable_set('menu_options_' . $xml['definition']['machine_name'], array());
    variable_set('menu_parent_' . $xml['definition']['machine_name'], 'main-menu:0');
    
    // fields & instances 
    
    $fields = $xml['fields'];
    foreach($fields as $field) {
      field_create_field($field);
    }

    $instances = $xml['instances'];
    foreach($instances as $instance) {
      unset($instance['id']);
      unset($instance['field_id']);
      $instance['entity_type'] = 'node';
      $instance['bundle'] = $content_type_name;
      field_create_instance($instance);
    }

    drush_print($content_type_name . " fields structure succesfully copied!     
    	Type «drush export-ct-desc " . $content_type_name . "» in order to see the structure of the new content type");
  }
  else return drush_set_error('ERROR_CODE', dt('Failed to open' . $file));
}

function drush_export_ct_delete($content_type_name)
{
  $_ = field_info_instances('node', $content_type_name);
  if (!empty($_)) {
    if (false == drush_confirm($content_type_name . ' will be deleted. Are you sure you want to continue?', $indent = 0)) return;
  }
  else {
    return drush_set_error('ERROR_CODE', dt('content ' . $content_type_name . ' has no fields or it doesn\'t exists'));
  }

  node_type_delete($content_type_name); //DANGER!!
  drush_print($content_type_name . " deleted succesfully ");
}
